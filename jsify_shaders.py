#!/usr/bin/python3

from __future__ import print_function
import os
import fnmatch
pathutils = os.path
from sys import version_info as _version_info
import sys
import re
from glob import glob
import argparse
import json
import re

# Specifying a pair looks like:
# // PAIR: <longname of shader>
# e.g: ' // PAIR: vert_passthru '

# Specifying a chain looks like:
# // CHAIN: <longname of shader 1> | <longname of shader 2> | ... | <longname of shader n>

# Specify 'special' shaders that need extra treatment before compiliation with
# // SPECIAL
# If CHAIN or PAIR specified put SPECIAL on second line

def main():
	args = parse_args()
	for path in args.srcpaths:
		if(not valid_path(path)):
			print("Path '{0}' is inaccessible! Skipping...".format(path), file=sys.stderr)
			break
		else:
			vertshaders, fragshaders, compshaders, geomshaders = gather_shaders(path)
	export_json(vertshaders, fragshaders, compshaders, geomshaders, args.output);

def gather_shaders(path):
	vslist = []
	fslist = []
	cslist = []
	gslist = []
	fullpath = pathutils.abspath(path)
	foundvert = foundfrag = foundcomp = foundgeom = []
	if(_version_info[0] < 3):
		foundvert = old_recurse(fullpath, "*.vs") + old_recurse(fullpath, "*.vert");
		foundfrag = old_recurse(fullpath, "*.fs") + old_recurse(fullpath, "*.frag");
		foundcomp = old_recurse(fullpath, "*.cs") + old_recurse(fullpath, "*.comp");
		foundgeom = old_recurse(fullpath, "*.gs") + old_recurse(fullpath, "*.geom");
	else:
		foundvert = glob(fullpath+"/**/*.vs", recursive = True) + glob(fullpath+"/**/*.vert", recursive = True)
		foundfrag = glob(fullpath+"/**/*.fs", recursive = True) + glob(fullpath+"/**/*.frag", recursive = True)
		foundcomp = glob(fullpath+"/**/*.cs", recursive = True) + glob(fullpath+"/**/*.comp", recursive = True)
		foundgeom = glob(fullpath+"/**/*.gs", recursive = True) + glob(fullpath+"/**/*.geom", recursive = True)

	for vs in foundvert:
		vslist.append(VertexShader(vs, fullpath = fullpath))
	for fs in foundfrag:
		fslist.append(FragmentShader(fs, fullpath = fullpath))
	for cs in foundcomp:
		cslist.append(ComputeShader(cs, fullpath = fullpath))
	for gs in foundgeom:
		gslist.append(GeometryShader(gs, fullpath = fullpath))

	return((vslist,fslist,cslist,gslist))

def export_json(vslist, fslist, cslist, gslist, opath):
	pairs = {}
	special = {}
	chains = {}
	genericlist = (vslist+fslist+gslist)

	handle_generic(genericlist, pairs, special, chains)

	for cs in cslist:
		if(cs.special): continue
		pairs[cs.extendedbasename] = [cs.__dict__] # Compute shaders are automatically added to pairs since they need none

	floating = {}

	for vs in vslist:
		floating[vs.longname] = vs.__dict__
	for fs in fslist:
		floating[fs.longname] = fs.__dict__
	for cs in cslist:
		floating[cs.longname] = cs.__dict__
	for gs in gslist:
		floating[gs.longname] = gs.__dict__


	ofile = open(opath,'w')
	ofile.write(json.dumps({
		"pairs": pairs,
		"all": floating,
		"chains": chains,
		"special": special
	},indent=2))
	ofile.close() 

def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("srcpaths", nargs='+', help="Path to directory full of vertex (.vs, .vert) and fragment (.fs, .frag) shader files")
	parser.add_argument("-o", "--output", default="assets/shaders.js", help="Path for output javascript file")
	return(parser.parse_args())

class Shader():
	def __init__(self, path, fullpath = "./"):
		self.__setup_vars__()
		self.__resolve_paths__(path, fullpath)
		self.__load_contents__(open(path, 'r'));

	def __load_contents__(self, file):
		for line in file:
			self.lines.append("      {0}".format(line))
			splitline = line.split()
			if(len(self.lines) == 1):
				self.pair = self.parse_pair(line)
				self.chain = self.parse_chain(line)
			if(len(self.lines) <= 2):
				self.special = re.match(R"^// SPECIAL$", line) != None;
			if(len(splitline) != 0):
				if(splitline[0] == "uniform"):
					self.uniforms.append([splitline[1],splitline[2].replace(';', '')])

		self.src = ''.join(self.lines)
		self.lines = len(self.lines)

	def __setup_vars__(self):
		self.type = None
		self.prefix = None
		self.pair = None
		self.lines = []
		self.uniforms = []
		self.chain = None
		self.special = False
		self.longname = None
		self.extendedbasename = None

	def __resolve_paths__(self, path, fullpath):
		self.filename = pathutils.split(path)[-1]
		self.name, self.ext = pathutils.splitext(self.filename)
		self.basename = pathutils.basename(self.name)
		self.name = "{0}_{1}".format(self.prefix, pathutils.basename(self.name))
		longprefix = [seg.strip() for seg in pathutils.relpath(path, start=fullpath).split(pathutils.sep)[0:-1] ]
		self.longname =  '_'.join(longprefix + [self.name])
		self.extendedbasename = '_'.join(longprefix + [self.basename])

	@staticmethod 
	def parse_pair(line):
		match = re.search(R"^// PAIR:\s+(\S+)\s*$", line);
		if(match != None):
			ref = match.groups()[-1].strip()
			return(ref);
		return(None);

	@staticmethod 
	def parse_chain(line):
		breakup = re.search(r"(// CHAIN:)(\s+.*)$", line)
		if(not breakup):
			return
		isolated = breakup.groups()[-1]
		splits = [element.strip() for element in re.split(r"\s+\|\s+", isolated)]
		return(splits if len(splits) > 0 else None)	

class VertexShader(Shader):
	def __setup_vars__(self):
		Shader.__setup_vars__(self)
		self.type = "vertex"
		self.prefix = "vert"

	def __load_contents__(self, file):
		self.lines = []
		self.uniforms = []
		self.attributes = []
		for line in file:
			self.lines.append("      {0}".format(line))
			splitline = line.split()
			if(len(self.lines) == 1):
				self.pair = self.parse_pair(line)
				self.chain = self.parse_chain(line)
			if(len(self.lines) == 2):
				self.special = re.match(R"^// SPECIAL$", line) != None;
			if(len(splitline) != 0):
				attmatch = re.search(R"layout\(location = [0-9]+\)", line)
				if(splitline[0] == "uniform"):
					self.uniforms.append([splitline[1],splitline[2].replace(';', '')])
				elif(attmatch != None):
					self.attributes.append([splitline[-2], splitline[-1].replace(';', '')])

		self.src = ''.join(self.lines)
		self.lines = len(self.lines)

class FragmentShader(Shader):
	def __setup_vars__(self):
		Shader.__setup_vars__(self)
		self.type = "fragment"
		self.prefix = "frag"

class GeometryShader(Shader):
	def __setup_vars__(self):
		Shader.__setup_vars__(self)
		self.type = "geometry"
		self.prefix = "geom"

class ComputeShader(Shader):
	def __setup_vars__(self):
		Shader.__setup_vars__(self)
		self.type = "compute"
		self.prefix = "comp"

def handle_generic(genericlist, pairs, special, chains):
	for generic in genericlist:
		result = None
		if(generic.pair != None):
			partner = find_by_longname(genericlist, generic.pair)
			if(partner == None):
				print("Couldn't find requested partner for {0}!".format(generic.extendedbasename), file=sys.stderr)
				continue
			result = (generic.__dict__, partner.__dict__)
			if(not generic.special): pairs[generic.extendedbasename] = result
		elif(generic.chain != None):
			result, error = resolve_chain(genericlist, generic)
			if(error):
				continue
			elif(not generic.special):
				chains[generic.extendedbasename] = result
		if(generic.special and result != None):
			special[generic.extendedbasename] = result
		elif(generic.special):
			special[generic.extendedbasename] = generic.__dict__

def resolve_chain(genericlist, generic):
	links = []
	error = False
	for linkname in generic.chain:
		if(linkname == "THIS"):
			links.append(generic)
		else:
			links.append(find_by_longname(genericlist, linkname))
	if(links.count(None) > 0):
		print("A link is missing in chain requested by {0}!".format(generic.extendedbasename), file=sys.stderr)
		error = True
	result = tuple(link.__dict__ for link in links)
	return(result, error)

def find_match(searchlist, value):
	index = None
	try:
		index = searchlist.index(value)
	except ValueError:
		return;
	return(searchlist[index])

def find_by_longname(searchlist, value):
	for shader in searchlist:
		if(shader.longname == value):
			return(shader)
	return(None)

def valid_path(path):
	try:
		os.listdir(path)
	except:
		return(False)
	return(True)

def old_recurse(path, pattern):
		matches = []
		itr = 0;
		for root, dirs, files in os.walk(path):
			for match in fnmatch.filter(files, pattern):
				matches.append(pathutils.join(root, match))
			itr+=1
		return(matches)

if __name__ == "__main__":
	main()