// PAIR: vert_passthru
#version 430 core

uniform layout(binding=0, r8) restrict image2DArray positionOutline;
uniform layout(binding=1, rgba16f) restrict writeonly image2DArray normalOutline; // What are we going to do with this once multiple layers are taken into account!?

in vec3 FragPos;
out vec4 fragColor;

void main(){
	ivec3 completeSize = imageSize(positionOutline); // normalOutline assumed same size
	ivec2 outlineSize = completeSize.xy;
	uint arraySize = completeSize.z;

	vec2 norm_dev_cord = (FragPos.xy + 1.0) * .5;

	for(int i = 0; i < arraySize; i++){
		imageStore(positionOutline, ivec3(norm_dev_cord*outlineSize, i), vec4(1.0, 0.0, 1.0, 1.0));
	}

	fragColor = vec4(vec3(float(completeSize.x == 0), float(completeSize.y == 0), float(completeSize.z == 0)), 1.0);
}