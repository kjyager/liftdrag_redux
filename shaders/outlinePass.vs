#version 330 core
layout (location = 0) in ivec2 aSliceIndex;

flat out ivec2 sliceindex;

void main()
{
	sliceindex = aSliceIndex;
}
