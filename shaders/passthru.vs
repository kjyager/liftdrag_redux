#version 330 core
layout (location = 0) in vec3 vertPos;

out vec3 FragPos;
flat out ivec2 twotup;

void main()
{
	FragPos = vertPos;
    gl_Position = vec4(vertPos, 1.0);
}
