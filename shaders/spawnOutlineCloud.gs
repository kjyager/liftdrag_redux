// CHAIN: vert_outlinePass | THIS | frag_drawPoints
// SPECIAL
#version 430 core

// REPLACE AT RUNTIME WITH REAL IMPLEMENTATION_MAX_INVOCATIONS, IMPLEMENTATION_MAX_VERTICES and IMPLEMENTATON_MAX_SSBO_BINDS
#define IMPLEMENTATION_MAX_INVOCATIONS 32
#define IMPLEMENTATION_MAX_VERTICES 64
#define IMPLEMENTATON_MAX_SSBO_BINDS -1

layout(points) in;
layout(invocations = IMPLEMENTATION_MAX_INVOCATIONS) in;
layout(points, max_vertices = IMPLEMENTATION_MAX_VERTICES) out;


in gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
} gl_in[];
flat in ivec2 sliceindex[];

out vec3 fragNor;

uniform layout(binding=0, rgba16f) restrict readonly image2DArray positionOutline;
uniform layout(binding=1, rgba16f) restrict readonly image2DArray normalOutline;


layout(binding = 0, std430) restrict readonly buffer outlineBlock{
	uvec2 coords[];
} outlineCoords[IMPLEMENTATON_MAX_SSBO_BINDS];

uniform uint coord_buffer_offset;
uniform uint outline_array_cap[IMPLEMENTATON_MAX_SSBO_BINDS];

uniform mat4 V;
uniform mat4 P;

uniform float time;

const uint uint_underflow = (uint(0) - uint(1));

void main(){

	uint sliceid = sliceindex[0].x;
	uint sourceid = sliceindex[0].y;
	uint coordindex = sliceid - coord_buffer_offset;

	uint points_done = gl_InvocationID*sourceid*IMPLEMENTATION_MAX_VERTICES;
	uint points_remaining = outline_array_cap[sliceid] - points_done;
	uint points_to_output = min(IMPLEMENTATION_MAX_VERTICES, points_remaining);
	uint points_start = points_done;

	for(uint p = 0; p < points_to_output; p++){

		ivec3 point_coord = ivec3(outlineCoords[coordindex].coords[points_start+p], sliceid);
		vec3 point_location = imageLoad(positionOutline, point_coord).xyz;
		fragNor = vec3(sliceid%2 == 0, sliceid%2 == 1, 0.0);
		// fragPos = (V * M * vec4(point_location, 1.0)).xyz;`
		gl_Position = (P * V * vec4(point_location, 1.0));
		EmitVertex();
	}

}
