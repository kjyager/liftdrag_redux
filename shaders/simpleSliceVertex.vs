#version 430 core 

layout(location = 0) in vec4 vertPos;
layout(location = 1) in vec4 vertNor;

out vec3 fragPos;
out vec3 fragNor;
out vec3 fragClipPos;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

uniform float nearpoint;
uniform float farpoint;

out float gl_ClipDistance[2];

void main(){
	gl_Position = (P * V * M * vertPos);
	fragPos = (M * vertPos).xyz;
	fragNor = (M * vertNor).xyz;
	fragClipPos = gl_Position.xyz;

	gl_ClipDistance[0] = nearpoint - vertPos.z;
	gl_ClipDistance[1] = vertPos.z - farpoint;
}