#version 430 core 

layout(location = 0) in vec4 vertPos;
layout(location = 1) in vec4 vertNor;

out vec3 fragPos;
out vec3 fragNor;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

void main(){
	gl_Position = (P * V * M * vertPos);
	fragPos = (V * M * vertPos).xyz;
	fragNor = (vertNor).xyz;
}