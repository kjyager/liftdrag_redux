// PAIR: vert_simpleSliceVertex
#version 430 core

uniform layout(binding=0, rgba16f) restrict image2DArray positionOutline;
uniform layout(binding=1, rgba16f) restrict writeonly image2DArray normalOutline; // What are we going to do with this once multiple layers are taken into account!?
layout(std430, binding=2) restrict writeonly buffer outlineBuffer{
	uvec2 outlineCoords[];
};
uniform layout(binding=3) atomic_uint atomicIndex;

uniform vec3 windvec;
uniform int layer;

in vec3 fragPos;
in vec3 fragNor;
in vec3 fragClipPos;

uniform float time;

out vec4 fragColor;

void main(){
	ivec3 completeSize = imageSize(positionOutline); // normalOutline assumed same size
	ivec2 outlineSize = completeSize.xy;
	uint arraySize = completeSize.z;

	vec2 NDC = (fragClipPos.xy + 1.0) / 2.0;
	ivec3 imageCoord = ivec3(NDC*outlineSize, layer); // Nearest neighbor 
	ivec3 prevCoord = imageCoord; prevCoord.z -= 1;

	vec3 prevPos = imageLoad(positionOutline, prevCoord).rgb * step(0, prevCoord.z);
	vec3 outlinePos = fragPos + fragNor + windvec + prevPos;

	imageStore(positionOutline, imageCoord, vec4(outlinePos, 1.0));
	imageStore(normalOutline, imageCoord, vec4(fragNor, 1.0));

	uint bufferidx = atomicCounterIncrement(atomicIndex);
	outlineCoords[bufferidx] = uvec2(imageCoord.xy);


	fragColor = vec4(fragNor, 1.0);
}