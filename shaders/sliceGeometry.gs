// @$ REPLACED AT RUNTIME WITH PROPER CONSTANTS @$ // Should define numslices

layout(triangle) in;
layout(triangle_strip, max_vertices = numslices*3) out;

out int gl_Layer;

in gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_out[];

out gl_ClipDistance[2]; 

uniform uint layeroffset;

void main(){

	float slicewidth = 2.0/numslices;

	int layer0 = int(gl_in[0].gl_Position.z/slicewidth);
	int layer1 = int(gl_in[1].gl_Position.z/slicewidth);
	int layer2 = int(gl_in[2].gl_Position.z/slicewidth);

	int minlayer = min(layer2, min(layer0, layer1));
	int maxlayer = max(layer2, max(layer0, layer1));
	
	for(int layer = minlayer; i <= maxlayer; i++){

		// This inner loop should get unrolled at compile time
		for(int i = 0; i < 3; i++){
			gl_out[i].gl_Position = gl_in[i].gl_Position; // Copy original vertex location 
			gl_ClipDistance[0] = gl_in[i].gl_Position.z - slicewidth*layer; // Set clipping with back plane of slice
			gl_ClipDistance[1] = slicewidth*(layer+1) - gl_in[i].gl_Position.z; // Set clipping with front plane of slice
			gl_Layer = layeroffset+layer; // Set correct layer for emitted primitive 
			EmitVertex();
		}
	}

}
