#version 330 core
layout (location = 0) in ivec2 aTwoTup;

out ivec2 twotup;

void main()
{
    twotup = aTwoTup;
}
