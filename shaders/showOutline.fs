// PAIR: vert_passthru
#version 430 core

#define INV(_F) (1.0 - (_F))
#define NOT(_F) INV(_F)

in vec3 FragPos;
out vec4 FragColor;

uniform float time;
uniform sampler2DArray posarray;
uniform sampler2DArray norarray;

void main()
{	
	vec2 NDC = (FragPos.xy + 1.0) * .5;

	float swap = step(mod(time, 5.0), 2.5);

	float numlayers = float(textureSize(posarray, 0).z);
	float linesize = ceil(sqrt(numlayers));
	vec2 scaled = NDC*linesize;

	vec3 gridtex = vec3(fract(scaled), floor(scaled.x)+linesize*floor(scaled.y));
	vec3 texel = texture(posarray, gridtex).rgb*swap + texture(norarray, gridtex).rgb*NOT(swap);
	vec3 color = texel * step(gridtex.z, numlayers-1);

	vec3 uvcol = vec3(gridtex.xy, gridtex.z/numlayers);

	FragColor = vec4(clamp(color, 0.0, 1.0), 1.0);
}