// CHAIN: vert_vistest | THIS | frag_drawPoints
// SPECIAL
#version 430 core

/* Tricky Implementation Note:
 * My Intel integrated GPU seems to have deviated from the OpenGL spec in terms of the meaning of
 * GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS, GL_MAX_GEOMETRY_OUTPUT_VERTICES, and GL_MAX_GEOMETRY_OUTPUT_COMPONENTS 
 * According to the spec the total output value should bound both vertices themselves and vertex attributes such
 * that adding more vertex shader outputs may reduce the number of vertices that can be written. However instead
 * the Igpu allows 256 vertices to be output even when the number of components exceeds 7. Yet the reported max
 * total output components is 1024 (the spec required minimum). This is a violation since 7*256 > 1024 which should 
 * be impossible.
*/

// REPLACE AT RUNTIME WITH REAL IMPLEMENTATION_MAX_VERTICES and IMPLEMENTATON_MAX_SSBO_BINDS
#define IMPLEMENTATION_MAX_INVOCATIONS 32
#define IMPLEMENTATION_MAX_VERTICES 256
#define IMPLEMENTATON_MAX_SSBO_BINDS -1

#define PI 3.14159
#define TURNS 2

layout(points) in;
layout(invocations = IMPLEMENTATION_MAX_INVOCATIONS) in;
layout(points, max_vertices = IMPLEMENTATION_MAX_VERTICES) out;

in gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
} gl_in[];
in ivec2 twotup[];

// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

out vec3 fragNor;
// out vec3 fragPos;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

void main(){

	vec2 ttconvert = vec2(twotup[0]);

	for(uint p = 0; p < IMPLEMENTATION_MAX_VERTICES; p++){
		float finalturns = TURNS+float(gl_InvocationID)/8.0;
		float incline = float(p)*(PI/IMPLEMENTATION_MAX_VERTICES);
		float azimuth = float(p)*((finalturns*2*PI)/IMPLEMENTATION_MAX_VERTICES);
		vec3 point_location = vec3(
			sin(incline)*cos(azimuth),
			sin(incline)*sin(azimuth),
			cos(incline)
		);

		fragNor = vec3(vec2(twotup[0]), 1.0);

		if(isnan(ttconvert.x) || isnan(ttconvert.y) || isinf(ttconvert.x) || isinf(ttconvert.y)){
			fragNor.b = .25;
		}else{
			point_location += vec3(ttconvert, 1.0);//1.0+(float(twotup[0].y)*.5);
		}
		
		
		// fragPos = (V * M * vec4(point_location, 1.0)).xyz;
		gl_Position = (P * V * M * vec4(point_location, 1.0));
		// gl_PointSize = 3.0;
		EmitVertex();
	}

}
