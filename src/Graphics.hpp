#pragma once
#ifndef GRAPHICS_H_
#define GRAPHICS_H_

#include <json.hpp>
#include "core.hpp"
#include "SimpleComponents.hpp"
#include <glm/glm.hpp>
#include <MatrixStack.h>

using namespace std;
using namespace glm;
using json = nlohmann::json;

namespace Graphics{
	static GLFWwindow* window;

	static int w_width = -1;
	static int w_height = -1;

	// Automatically setup quad VAO/VBO for full screen pixel shaders
	static GLuint quadVAO = 0;
	static GLuint quadVBO = 0;

	static ShaderLibrary* shaderlib = nullptr; // Pointer to the shader library. 

	static GLuint throwawayFBO = 0;
	static GLuint throwawayTex = 0;

	static GLuint cleantex = 0;

	struct MVPset{
		MatrixStack M;
		MatrixStack V;
		MatrixStack P;
	};

	/* Structure for a single pass of slicing through the mesh. */
	struct SliceSet{
		uint numslices = 0;
		double slicewidth = 0;
		GLuint outlinePos = 0; // Texture array of textures holding position information in RGBAF16 or RGBAF32
		GLuint outlineNor = 0; // Texture array of textures holding normal information in RGBAF16 or RGBAF32
		
		// Array of SSBO holding image coords for above textures e.g outlineCoords[i][x] yields ivec2 coord for outlinePos/Nor layer i
		vector<GLuint> outlineCoords; 
		vector<GLuint> atomicIndex; // Keeps track of the last index for each SSBO.
	};
	static SliceSet sliceset; // Default instance for use in namespace. 

	struct SliceVisual {
		GLuint VAO = 0;
		GLuint posVBO = 0;
		GLuint idxVBO = 0;

		int max_points_per_primative = 0;
		int max_output_vertices = 0;
		int max_invocations;
		int freeBindings = 0; // Available SSBO bindings
		uint totalSources = 0;

		// See the visualizeOutline code for information on these
		vector<uint> indexCaps;
		vector<uint> sliceSources;
		vector<uint> chunkSwaps;
		vector<uint> chunkSources;

		// Rudimentary orbit camera instance. It's a hack to use it here. 
		OrbitCamera orbitcam;
		dvec2 oldmouse;
	};
	static SliceVisual visual; // Defualt instance for use in the namespace 

	// Placeholder for accelerated slicer in the future. 
	struct SliceProgram{
		Program sliceprogram;
		uint invocations;
	};


	/** Setup variables, structs, and Opengl objects */
	void init(Application* appstate, uint numslices);

	/** Setup the special shader for point cloud outline visualization by injecting runtime
	 * variables into pre-proc definitions within the GLSL. This is specialized specifically
	 * for this code, and should really be implemented as a general feature of my shader library
	 * instead. Don't give it too much consideration, as it's messy and rushed.
	 */
	void initSpecialShaders(const json& special);

	/** Does the slicing and basic outline calculation
	 * First it sets up a simple ortho camera for the scene, then sets two custom clipping
	 * planes which will get moved iteratively across the bounds of the view. Right now the
	 * size of the frustum is based on the fact that mesh objects are normalized within my
	 * Geometry class to fit in -1 to 1, and in a more robust application could be automatically
	 * set using bounding box or bounding sphere information of the mesh. 
	 *
	 * Each iteration the function moves the clipping planes and re-draws with the outlining
	 * shader program bound. It also binds the correct layer of the texture buffers used to store
	 * outlining data at each iteration so that results are drawn into the correct buffer. 
	 */ 
	void formOutline(const SolidMesh& mesh, uint numslices, glm::vec3 windvec);

	/**
	 * Determines the number of geometry shader invocations and prepares the information
	 * and Opengl objects needed to accomplish the point cloud visualization. 
	 */
	void setupVisualization();

	/** Draws point cloud visualization. This function is a huge mass of frustrating
	 * iteration and geometry shader limitation work-arounds. I recommend ignoring it
	 * if at all possible. Ultimately it's only purpose is to invoke the "spawnOutlineCloud.gs"
	 * shader many times, and to update the orbit camera. 
	 */
	void visualizeOutline(double dt, const SolidMesh& mesh);

	/** Draw a simple 2D view of each slice after running formOutlien() */
	void testVisual(); 

	/** Clears slice buffers */ 
	void cleanOutline();

	void onResize(GLFWwindow* window, int width, int height);

	/** Draw full-screen quad */
	void drawFSQuad();

	/** Print a bunch of OpenGL runtime constants */
	void printRelevantConstants();
}


#endif