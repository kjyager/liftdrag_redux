#include "Graphics.hpp"
#include <limits.h>
#include <cmath>
#include <unordered_map>
#include <sstream>
#include <iostream>
#include <fstream>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include <glad/glad.h>

#define SLICE_SIZE 1024
#define OUTLINE_ARRAY_SIZE 100000
#define OUTLINE_GEOMETRY_OUTPUT_COMPONENTS 7

// #define DUMPONOVERFLOW

static void genSliceSet(Graphics::SliceSet& sliceset, uint numslices);
static void drawGeometry(const Geometry &geomcomp, Graphics::MVPset &MVP, Program* shader);
static void initQuad(GLuint& quadVAO, GLuint& quadVBO);
static void drawPointArrays(const vector<GLuint>& VAO, const vector<GLuint>& VBO, Graphics::MVPset &MVP, Program* shader);
static void createSingleComponentFloatTexture(GLuint& tex, GLuint x, GLuint y, GLenum minfilter, GLenum magfilter);
static void init_clear_tex(GLuint* cleantex, uint numslices);
static void createTexture(
	GLuint& tex, GLuint x, GLuint y, GLenum internalFormat = GL_RGB,
	GLenum format = GL_RGB, GLenum type = GL_UNSIGNED_BYTE, GLenum minfilter = GL_LINEAR,
	GLenum magfilter = GL_LINEAR
);
static void createImageTexture(GLuint& texid, uint x, uint y, UCHAR* data);

void Graphics::init(Application* appstate, uint numslices){
	// Save window and shader library locally for easy access
	glfwGetFramebufferSize(appstate->window, &w_width, &w_height);
	glViewport(0, 0, w_width, w_height);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	shaderlib = &appstate->shaderlib;

	// Init full screen quad used by drawFSQuad()
	initQuad(quadVAO, quadVBO);

	// Create throwaway FBO and TEX for behind the scenes renders
	glGenFramebuffers(1, &throwawayFBO);
	createTexture(throwawayTex, SLICE_SIZE, SLICE_SIZE);
	glBindFramebuffer(GL_FRAMEBUFFER, throwawayFBO);
	glBindTexture(GL_TEXTURE_2D, throwawayTex);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, throwawayTex, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (!GL_ARB_clear_texture) {
		init_clear_tex(&cleantex, numslices);
	}

	// Fill structure core to the slicing and outlining operation.
	genSliceSet(sliceset, numslices);
	setupVisualization();

	window = appstate->window;
}

void Graphics::initSpecialShaders(const json& special) {
	size_t ilen1 = strlen("#define IMPLEMENTATION_MAX_VERTICES") + 24;
	size_t ilen2 = strlen("#define IMPLEMENTATON_MAX_SSBO_BINDS") + 24;
	size_t ilen3 = strlen("#define IMPLEMENTATION_MAX_INVOCATIONS") + 24;

	char* insert1 = new char[ilen1];
	char* insert2 = new char[ilen2];
	char* insert3 = new char[ilen3];
	snprintf(insert1, ilen1, "#define IMPLEMENTATION_MAX_VERTICES %d", visual.max_output_vertices);
	snprintf(insert2, ilen2, "#define IMPLEMENTATON_MAX_SSBO_BINDS %d", visual.freeBindings);
	snprintf(insert3, ilen3, "#define IMPLEMENTATION_MAX_INVOCATIONS %d", visual.max_invocations);

	if (special.count("spawnOutlineCloud") != 1) {
		fprintf(stderr, "Missing required special shader chain spawnOutlineCloud in JSON file!\n");
		assert(false);
	}
	json cloudcopy = special["spawnOutlineCloud"];
	for (json::iterator it = cloudcopy.begin(); it != cloudcopy.end(); it++) {
		if (it.value()["type"].get<string>() == "geometry") {
			size_t start, sublength = string::npos;
			string src = it.value()["src"].get<string>();
			if ((start = src.find("#define IMPLEMENTATION_MAX_VERTICES")) == string::npos || (sublength = src.find_first_of('\n', start) - start) == string::npos) {
				fprintf(stderr, "Could't find key preprocessor varaible to replace!\n");
			}
			else {
				src.replace(start, sublength, insert1);
			}

			if ((start = src.find("#define IMPLEMENTATON_MAX_SSBO_BINDS")) == string::npos || (sublength = src.find_first_of('\n', start) - start) == string::npos) {
				fprintf(stderr, "Could't find key preprocessor varaible to replace!\n");
			}
			else {
				src.replace(start, sublength, insert2);
			}

			if ((start = src.find("#define IMPLEMENTATION_MAX_INVOCATIONS")) == string::npos || (sublength = src.find_first_of('\n', start) - start) == string::npos) {
				fprintf(stderr, "Could't find key preprocessor varaible to replace!\n");
			}
			else {
				src.replace(start, sublength, insert3);
			}
			it.value()["src"] = src;
		}
	}
	ofstream clouddump("jsondump.json", ofstream::out);
	clouddump << std::setw(3) << cloudcopy << endl;
	clouddump.close();

	if (!shaderlib->buildAndAdd("spawnOutlineCloud", cloudcopy)) {
		fprintf(stderr, "Missing required special shader chain spawnOutlineCloud\n");
		assert(false);
	}
}

static uint frames = 0;
void Graphics::formOutline(const SolidMesh& mesh, uint numslices, glm::vec3 windvec){
	glViewport(0, 0, SLICE_SIZE, SLICE_SIZE);
	glBindFramebuffer(GL_FRAMEBUFFER, throwawayFBO);

	// Setup overall view frustum
	MVPset MVP;
	StaticCamera cam = StaticCamera(90, vec3(0.0, 0.0, 1.0), vec3(0,0,0));
	cam.near = 0.0;
	cam.far = 2.0;
	MVP.V = MatrixStack(cam.getView());
	MVP.P = MatrixStack(cam.getOrthographic());

	// Enable two custom clipping planes for slicing
	glEnable(GL_CLIP_PLANE0);
	glEnable(GL_CLIP_PLANE1);

	// Bind outliner shader
	Program* slicerprog = shaderlib->getPtr("naiveOutliner");
	shaderlib->fastActivate(slicerprog);

	glActiveTexture(GL_TEXTURE0);
	glBindImageTexture(0, sliceset.outlinePos, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16F);
	glActiveTexture(GL_TEXTURE1);
	glBindImageTexture(1, sliceset.outlineNor, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16F);

	// Iterate over slices
	for(uint i = 0; i < numslices; i++){
		// Bind SSBO and atomic counter 
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, sliceset.outlineCoords[i]);
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 3, sliceset.atomicIndex[i]);

		// Assign output layer and send in slice boundaries
		glUniform1i(slicerprog->getUniform("layer"), i);
		glUniform3f(slicerprog->getUniform("windvec"), windvec.x, windvec.y, windvec.z);
		glUniform1f(slicerprog->getUniform("nearpoint"), 1.0 - sliceset.slicewidth*i);
		glUniform1f(slicerprog->getUniform("farpoint"), 1.0 - sliceset.slicewidth*(i+1));
		ASSERT_NO_GLERR();

		// Draw calls on geometry
		for(const Geometry& geo : mesh.geometrylist){
			drawGeometry(geo, MVP, slicerprog);
		}

		// ============ This block is purely for debugging purposes and can be ignored =============
		{
			glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, sliceset.atomicIndex[i]);
			GLuint finalcount;
			glGetBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint), &finalcount); 
			if(finalcount >= OUTLINE_ARRAY_SIZE){
				fprintf(stderr, "Warning! Final index seems to be overflow!\n");
				fprintf(stderr, "Final index for slice %d was %u\n", i, finalcount);
				#ifdef DUMPONOVERFLOW
					char fname[strlen("SSBOdump_%d_end%u.bin") + 4];
					sprintf(fname, "SSBOdump_%d_end%u.bin", i, finalcount);
					GLuint* buffer = (GLuint*) calloc(OUTLINE_ARRAY_SIZE, 2*sizeof(GLuint));
					glBindBuffer(GL_SHADER_STORAGE_BUFFER, sliceset.outlineCoords[i]);
					glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(GLuint)*2*OUTLINE_ARRAY_SIZE, buffer);
					FILE* f = fopen(fname, "wb");
					if(f){
						fwrite(buffer, OUTLINE_ARRAY_SIZE, sizeof(GLuint)*2, f);
						fclose(f);
					}
					glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
				#endif
			}
			glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
		} 
		// ============ This block is purely for debugging purposes and can be ignored =============
	}
	

	glDisable(GL_CLIP_PLANE0);
	glDisable(GL_CLIP_PLANE1);

	// Unbind images and buffers
	glBindImageTexture(1, 0, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16F);
	glActiveTexture(GL_TEXTURE0);
	glBindImageTexture(0, 0, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16F);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 3, 0);

	// Leave things how we found them
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, w_width, w_height);
}


static GLuint testpointVAO; 
static GLuint testpointVBO;
void Graphics::setupVisualization() {
	// Query and compute geometry shader limitations
	int max_out_components;
	int max_out_verts;
	glGetIntegerv(GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS, &max_out_components);
	glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_VERTICES, &max_out_verts);
	glGetIntegerv(GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS, &(visual.freeBindings));
	glGetIntegerv(GL_MAX_GEOMETRY_SHADER_INVOCATIONS, &(visual.max_invocations));
	visual.max_points_per_primative = (max_out_components / OUTLINE_GEOMETRY_OUTPUT_COMPONENTS) * visual.max_invocations;
	visual.max_output_vertices = MIN(max_out_components / OUTLINE_GEOMETRY_OUTPUT_COMPONENTS, max_out_verts);

	glGenVertexArrays(1, &(visual.VAO));
	glGenBuffers(1, &(visual.idxVBO));


	uint upperidxbound = (OUTLINE_ARRAY_SIZE / visual.max_points_per_primative + 1) * sliceset.numslices * sizeof(GLint) * 2;
	fprintf(stderr, "Upper bound of CPU side visualization buffer computed as %d bytes\n", upperidxbound);
	glBindVertexArray(visual.VAO);
	glBindBuffer(GL_ARRAY_BUFFER, visual.idxVBO);
	glBufferData(GL_ARRAY_BUFFER, upperidxbound, nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	visual.orbitcam = OrbitCamera(90, vec3(3.0, 0.0, 0.0), vec3(0.0));
	visual.oldmouse = dvec2(-1, -1);

	// Below is unused test code, ignore it. 
	static const float centerpoint[] = {0.0f, 0.0f, 0.0f};
	static const int twotuple[] = {0, 1, 1, 0};

	glGenVertexArrays(1, &testpointVAO);
	glGenBuffers(1, &testpointVBO);
	glBindVertexArray(testpointVAO);
	glBindBuffer(GL_ARRAY_BUFFER, testpointVBO);
	glBufferData(GL_ARRAY_BUFFER, 4*sizeof(GLint), twotuple, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Graphics::testVisual(){
	dvec2 deltamouse, newmouse;
	glfwGetCursorPos(window, &newmouse.x, &newmouse.y);
	deltamouse = newmouse - visual.oldmouse;
	visual.oldmouse = newmouse;

	Program* normaldraw = shaderlib->getPtr("normalDraw");
	shaderlib->fastActivate(normaldraw);
	visual.orbitcam.near = 0.001f;
	visual.orbitcam.far = 100.0f;
	vec2 spinvector = vec2(-deltamouse.x / w_width * 4.0f, deltamouse.y / w_height * 4.0f);
	MVPset MVP;
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE)) {
		visual.orbitcam.updateSpin(spinvector);
	}
	if (glfwGetKey(window, GLFW_KEY_KP_ADD)) {
		visual.orbitcam.pose.loc += visual.orbitcam.getViewDir()*.05f;
	}
	if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT)) {
		visual.orbitcam.pose.loc += visual.orbitcam.getViewDir()*-.05f;
	}
	MVP.V = visual.orbitcam.getView();
	MVP.P = visual.orbitcam.getPerspective(static_cast<double>(w_width) / w_height);

	CHECKED_GL_CALL(glPointSize(2.0f));

	Program* drawcloud = shaderlib->getPtr("testPoints");
	shaderlib->fastActivate(drawcloud);

	CHECKED_GL_CALL(glUniformMatrix4fv(drawcloud->getUniform("M"), 1, GL_FALSE, value_ptr(MVP.M.topMatrix())));
	CHECKED_GL_CALL(glUniformMatrix4fv(drawcloud->getUniform("V"), 1, GL_FALSE, value_ptr(MVP.V.topMatrix())));
	CHECKED_GL_CALL(glUniformMatrix4fv(drawcloud->getUniform("P"), 1, GL_FALSE, value_ptr(MVP.P.topMatrix())));

	glBindVertexArray(testpointVAO);
	glBindBuffer(GL_ARRAY_BUFFER, testpointVBO);
	GLuint attrib_handle = drawcloud->getAttribute("aTwoTup");
	GLSL::enableVertexAttribArray(attrib_handle);
	CHECKED_GL_CALL(glVertexAttribIPointer(attrib_handle, 2, GL_INT, 0, NULL));
	glDrawArrays(GL_POINTS, 0, 2);
	GLSL::disableVertexAttribArray(attrib_handle);
	glBindVertexArray(0);
	glPointSize(1.0f);


}

void Graphics::visualizeOutline(double dt, const SolidMesh& mesh) {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	CHECKED_GL_CALL(glBindVertexArray(visual.VAO));
	CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, visual.idxVBO));

	/* Create a 'source' vertex by splitting the total number of points by the maximum geometry shader output vertices.
	 * Then create a slice index value for each of these sources. The slice index value identifies 
	 * ( the_index_of_the_original_slice, the_index_of_the_source_within_the_slice) as an ivec2 bound by [0,0] and (NUMBER_OF_SLICES, ?)
	 * Then buffer these slice index values into the corresponding array buffer. Finally, keep track of the number of slices iterated
	 * through and take note of when a chunk swap will need to occur. A chunk swap is needed because a limited number of SSBO can be bound
	 * at a time. As a result we must swap out which buffers are bound as a part of the rendering process. 
	*/
	visual.totalSources = 0;
	visual.chunkSources.clear();
	visual.chunkSwaps.clear();
	visual.sliceSources.resize(sliceset.numslices, 0);
	visual.indexCaps.resize(sliceset.numslices, 0);
	uint source_accum = 0;
	uint start = 0;
	for (int i = 0; i < sliceset.numslices; i++) {
		// When i % freeBindings == 0 we will need to do a chunk swap and rebind all the SSBOs. We make a note of when we hit this point here. 
		if ((i % visual.freeBindings == 0 && i != start)) {
			//fprintf(stderr, "%d %d\n", source_accum, i);
			visual.chunkSources.push_back(source_accum); // Total Number of sources in the chunk
			visual.chunkSwaps.push_back(start); // Slice index for the chunk swap
			start = i;
			source_accum = 0;
		}

		// Get the number of outline points in slice 'i' from the slice's atomic counter
		GLuint count;
		CHECKED_GL_CALL(glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, sliceset.atomicIndex[i]));
		CHECKED_GL_CALL(glGetBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint), &count));
		visual.indexCaps[i] = count; // Keep track of final index for later use. 

		// Calculate the number of source vertices needed to spawn all the outline points for this slice. Almost always (count/max_output_vertices) + 1
		visual.sliceSources[i] = (count / visual.max_points_per_primative) + ((count % visual.max_points_per_primative != 0) ? 1 : 0);

		// Generate a slice index ivec2 for each source vertex in this slice.
		GLuint sources = visual.sliceSources[i];

		ivec2* data = new ivec2[sources];
		for (int j = 0; j < sources; j++) {
			data[j] = ivec2(i, j);
		}

		// Buffer new slice index variables into corresponding buffer using an offset formed by the total number of sources so far.
		size_t suboffset = visual.totalSources * sizeof(GLint) * 2;
		size_t subrange = sources * sizeof(GLint) * 2;
		CHECKED_GL_CALL(glBufferSubData(GL_ARRAY_BUFFER, suboffset, subrange, &(data[0].x)));
		visual.totalSources += sources; // Counting total sources over all slices
		source_accum += sources; // Counting total sources over this chunk
	}
	//fprintf(stderr, "%d %d\n", source_accum, sliceset.numslices-1);
	visual.chunkSources.push_back(source_accum); // Total Number of sources in the chunk
	visual.chunkSwaps.push_back(start); // Slice index for the chunk swap
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	CHECKED_GL_CALL(glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0));
	
	
	// Draw points and model
	dvec2 deltamouse, newmouse;
	glfwGetCursorPos(window, &newmouse.x, &newmouse.y);
	deltamouse = newmouse - visual.oldmouse;
	visual.oldmouse = newmouse;

	Program* normaldraw = shaderlib->getPtr("normalDraw");
	shaderlib->fastActivate(normaldraw);

	vec2 spinvector = vec2(-deltamouse.x / w_width * 4.0f, deltamouse.y / w_height * 4.0f);
	MVPset MVP;
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE)) {
		visual.orbitcam.updateSpin(spinvector);
	}
	if (glfwGetKey(window, GLFW_KEY_KP_ADD) || glfwGetKey(window, GLFW_KEY_EQUAL)) {
		visual.orbitcam.pose.loc += visual.orbitcam.getViewDir()*.05f;
	}
	if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) || glfwGetKey(window, GLFW_KEY_MINUS)) {
		visual.orbitcam.pose.loc += visual.orbitcam.getViewDir()*-.05f;
	}
	MVP.V = visual.orbitcam.getView();
	MVP.P = visual.orbitcam.getPerspective(static_cast<double>(w_width) / w_height);

	if(!glfwGetKey(window, GLFW_KEY_Z)){
		for (const Geometry& geo : mesh.geometrylist) {
			drawGeometry(geo, MVP, normaldraw);
		}
	}
	


	/* Setup the shader and basic attributes and uniforms for visualizing the slices
	*/
	Program* drawcloud = shaderlib->getPtr("spawnOutlineCloud");
	shaderlib->fastActivate(drawcloud);
	CHECKED_GL_CALL(glActiveTexture(GL_TEXTURE0));
	glBindImageTexture(0, sliceset.outlinePos, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA16F);
	CHECKED_GL_CALL(glActiveTexture(GL_TEXTURE1));
	glBindImageTexture(1, sliceset.outlineNor, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA16F);

	CHECKED_GL_CALL(glPointSize(2.0f));

	CHECKED_GL_CALL(glUniformMatrix4fv(drawcloud->getUniform("V"), 1, GL_FALSE, value_ptr(MVP.V.topMatrix())));
	CHECKED_GL_CALL(glUniformMatrix4fv(drawcloud->getUniform("P"), 1, GL_FALSE, value_ptr(MVP.P.topMatrix())));

	CHECKED_GL_CALL(glBindVertexArray(visual.VAO));
	CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, visual.idxVBO));
	GLuint indexhandle = drawcloud->getAttribute("aSliceIndex");
	CHECKED_GL_CALL(GLSL::enableVertexAttribArray(indexhandle));
	CHECKED_GL_CALL(glVertexAttribIPointer(indexhandle, 2, GL_INT, 0, NULL));
	

	/* Draw the point cloud for all the slices. This job is broken up into a number of chunks determined prior.
	 * Each chunk involves drawing many source vertices associated with multiple slices from the set. Each source
	 * vertex is used by the geometry shader as an input which will then spawn many new points whose positions and
	 * properties are determined by the data in the slice buffer images. This job must be broken up into chunks
	 * because the number of SSBO binding points is often less than the number of slices, and each slice has its
	 * own SSBO for storing the image coordinates of it's outlined points. 
	*/
	int slicebase = 0; // The first index (0 - NUMBER_OF_SLICES) that this chunk services. 
	GLsizei offset = 0; // Overall index into the array of source vertices
	for (int i = 0; i < visual.chunkSwaps.size(); i++) {
		glUniform1ui(drawcloud->getUniform("coord_buffer_offset"), visual.chunkSwaps[i]);
		// Use all available SSBO binding points to bind the coordinate buffers for this chunk's slices. 
		for (int k = 0; k < visual.freeBindings && visual.chunkSwaps[i]+k < sliceset.numslices; k++) {
			CHECKED_GL_CALL(glBindBufferBase(GL_SHADER_STORAGE_BUFFER, k, sliceset.outlineCoords[visual.chunkSwaps[i]+k]));
		}

		glUniform1uiv(drawcloud->getUniform("outline_array_cap"), visual.freeBindings, visual.indexCaps.data()); // Give the last valid index into the SSBO as a uniform array (one per slice)
		// glUniform1f(drawcloud->getUniform("time"), glfwGetTime());
		
		// Calculate the range of points to send through the pipeline by multiplying the number of sources by the size of each sliceindex (2*GLint)
		GLsizei range = visual.chunkSources[i];
		CHECKED_GL_CALL(glDrawArrays(GL_POINTS, offset, range));
		offset += range; // Add to total offset into VAO
	}
	// CHECKED_GL_CALL(GLSL::disableVertexAttribArray(poshandle));
	CHECKED_GL_CALL(GLSL::disableVertexAttribArray(indexhandle));
	CHECKED_GL_CALL(glPointSize(1.0f));
	for (int i = 0; i < visual.freeBindings; i++) {  CHECKED_GL_CALL(glBindBufferBase(GL_SHADER_STORAGE_BUFFER, i, 0));  }
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	ASSERT_NO_GLERR();
}

void Graphics::cleanOutline() {
	for (int i = 0; i < sliceset.numslices; i++) {
		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, sliceset.atomicIndex[i]);
		glInvalidateBufferData(sliceset.atomicIndex[i]);
		glClearBufferData(GL_ATOMIC_COUNTER_BUFFER, GL_R8, GL_RED, GL_UNSIGNED_INT, nullptr);
	}

	if (!GL_ARB_clear_texture) {
		// This is a huge pain to set up
		CHECKED_GL_CALL(glCopyImageSubData(cleantex, GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, sliceset.outlinePos, GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, SLICE_SIZE, SLICE_SIZE, sliceset.numslices));
		CHECKED_GL_CALL(glCopyImageSubData(cleantex, GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, sliceset.outlineNor, GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, SLICE_SIZE, SLICE_SIZE, sliceset.numslices));
	}else {
		// Easier but not necessarily available before OpenGL 4.4
		glClearTexImage(sliceset.outlinePos, 0, GL_RGBA, GL_HALF_FLOAT, nullptr);
		glClearTexImage(sliceset.outlineNor, 0, GL_RGBA, GL_HALF_FLOAT, nullptr);
	}
}


void Graphics::onResize(GLFWwindow* window, int width, int height){
	w_width = width;
	w_height = height;
	glViewport(0, 0, width, height);
}

void Graphics::drawFSQuad(){
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}

void Graphics::printRelevantConstants(){
	int infoarray[6] = {0,0,0,0,0,0};
	int MAX_COMPUTE_WORK_GROUP_INVOCATIONS; glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &MAX_COMPUTE_WORK_GROUP_INVOCATIONS);
	int max_color_attachments; glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &max_color_attachments);
	int maxSSBObinds; glGetIntegerv(GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS, &maxSSBObinds);
	int maxSSBOfragblocks; glGetIntegerv(GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS, &maxSSBOfragblocks);
	int maxSSBOgeoblocks; glGetIntegerv(GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS, &maxSSBOgeoblocks);
	int maxSSBOcompblocks; glGetIntegerv(GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS, &maxSSBOcompblocks);
	int maxArrayLayers; glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxArrayLayers);
	int maxGeomOutVerts;  glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_VERTICES, &maxGeomOutVerts);
	int maxGeomOutComponents;  glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_COMPONENTS, &maxGeomOutComponents);
	int maxTotalGeomOutComponents;  glGetIntegerv(GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS, &maxTotalGeomOutComponents);
	int maxGeomInvocations;  glGetIntegerv(GL_MAX_GEOMETRY_SHADER_INVOCATIONS, &maxGeomInvocations);
	

	printf("MAX_COMPUTE_WORK_GROUP_INVOCATIONS: %d\n", MAX_COMPUTE_WORK_GROUP_INVOCATIONS);
	printf("GL_MAX_COLOR_ATTACHMENTS: %d\n", max_color_attachments);
	printf("GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS: %d\n", maxSSBObinds);
	printf("GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS: %d\n", maxSSBOfragblocks);
	printf("GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS: %d\n", maxSSBOgeoblocks);
	printf("GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS: %d\n", maxSSBOcompblocks);
	printf("GL_MAX_ARRAY_TEXTURE_LAYERS: %d\n", maxArrayLayers);
	printf("GL_MAX_GEOMETRY_OUTPUT_VERTICES: %d\n", maxGeomOutVerts);
	printf("GL_MAX_GEOMETRY_OUTPUT_COMPONENTS: %d\n", maxGeomOutComponents);
	printf("GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS: %d\n", maxTotalGeomOutComponents);
	printf("GL_MAX_GEOMETRY_SHADER_INVOCATIONS: %d\n", maxGeomInvocations);
}

static void genSliceSet(Graphics::SliceSet& sliceset, uint numslices){
	// Generate texture arrays for position and normals
	glGenTextures(1, &(sliceset.outlinePos));
	glBindTexture(GL_TEXTURE_2D_ARRAY, sliceset.outlinePos);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA16F, SLICE_SIZE, SLICE_SIZE, numslices);

	glGenTextures(1, &(sliceset.outlineNor));
	glBindTexture(GL_TEXTURE_2D_ARRAY, sliceset.outlineNor);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA16F, SLICE_SIZE, SLICE_SIZE, numslices);
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

	// Create vectors of SSBO and atomic counter objects
	sliceset.outlineCoords.resize(numslices, 0);
	sliceset.atomicIndex.resize(numslices, 0);
	glGenBuffers(numslices, sliceset.outlineCoords.data());
	glGenBuffers(numslices, sliceset.atomicIndex.data());
	for(int i = 0; i < numslices; i++){
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, sliceset.outlineCoords[i]);
		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(GLuint)*2*OUTLINE_ARRAY_SIZE, nullptr, GL_STATIC_DRAW);
		glClearBufferData(GL_SHADER_STORAGE_BUFFER, GL_RG8, GL_RG, GL_UNSIGNED_INT, nullptr);

		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, sliceset.atomicIndex[i]);
		glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(GLuint), nullptr, GL_STATIC_DRAW);
		glClearBufferData(GL_ATOMIC_COUNTER_BUFFER, GL_R8, GL_RED, GL_UNSIGNED_INT, nullptr);
	}
	ASSERT_NO_GLERR();
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);


	sliceset.numslices = numslices;
	sliceset.slicewidth = 2.0/numslices;
}

static void drawGeometry(const Geometry &geomcomp, Graphics::MVPset &MVP, Program* shader){
	int h_pos, h_nor, h_tex;
	h_pos = h_nor = h_tex = -1;

	glUniformMatrix4fv(shader->getUniform("M"), 1, GL_FALSE, value_ptr(MVP.M.topMatrix()));
	glUniformMatrix4fv(shader->getUniform("V"), 1, GL_FALSE, value_ptr(MVP.V.topMatrix()));
	glUniformMatrix4fv(shader->getUniform("P"), 1, GL_FALSE, value_ptr(MVP.P.topMatrix()));

	ASSERT_NO_GLERR();
	glBindVertexArray(geomcomp.vaoID);
	ASSERT_NO_GLERR();
	// Bind position buffer
	h_pos = shader->getAttribute("vertPos");
	ASSERT_NO_GLERR();
	GLSL::enableVertexAttribArray(h_pos);
	ASSERT_NO_GLERR();
	glBindBuffer(GL_ARRAY_BUFFER, geomcomp.posBufID);
	ASSERT_NO_GLERR();
	glVertexAttribPointer(h_pos, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);

	ASSERT_NO_GLERR();
	// Bind normal buffer
	h_nor = shader->getAttribute("vertNor");
	if(h_nor != -1 && geomcomp.norBufID != 0) {
		GLSL::enableVertexAttribArray(h_nor);
		glBindBuffer(GL_ARRAY_BUFFER, geomcomp.norBufID);
		glVertexAttribPointer(h_nor, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);
	}

	if (geomcomp.texBufID != 0) {
		// Bind texcoords buffer
		h_tex = shader->getAttribute("vertTex");
		if(h_tex != -1 && geomcomp.texBufID != 0) {
			GLSL::enableVertexAttribArray(h_tex);
			glBindBuffer(GL_ARRAY_BUFFER, geomcomp.texBufID);
			glVertexAttribPointer(h_tex, 2, GL_FLOAT, GL_FALSE, 0, (const void *)0);
		}
	}

	ASSERT_NO_GLERR();
	// Bind element buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geomcomp.eleBufID);
	ASSERT_NO_GLERR();
	// Draw
	glDrawElements(GL_TRIANGLES, (int)geomcomp.eleBuf->size(), GL_UNSIGNED_INT, (const void *)0);
	
	// Disable and unbind
	if(h_tex != -1) {
	GLSL::disableVertexAttribArray(h_tex);
	}
	if(h_nor != -1) {
	GLSL::disableVertexAttribArray(h_nor);
	}
	GLSL::disableVertexAttribArray(h_pos);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

static void init_clear_tex(GLuint* cleantex, uint numslices){
	glGenTextures(1, cleantex);
	glBindTexture(GL_TEXTURE_2D_ARRAY, *cleantex);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);

	uint ntexels = SLICE_SIZE * SLICE_SIZE*numslices;
	size_t zsize = ntexels*4*sizeof(GLhalf); 
	GLhalf* zeros = (GLhalf*) calloc(ntexels, 4*sizeof(GLhalf));
	if (zeros == NULL) {
		fprintf(stderr, "Failed to allocate clear data for texture array!\n");
		exit(EXIT_FAILURE);
	}
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA16F, SLICE_SIZE, SLICE_SIZE, numslices, 0, GL_RGBA, GL_HALF_FLOAT, zeros);
	free(zeros);
}

static void createSingleComponentFloatTexture(GLuint& tex, GLuint x, GLuint y, GLenum minfilter, GLenum magfilter){
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minfilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magfilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, x, y, 0, GL_RED, GL_FLOAT, nullptr);

	ASSERT_NO_GLERR();
}

static void createTexture(
	GLuint& tex, GLuint x, GLuint y, GLenum internalFormat,
	GLenum format, GLenum type, GLenum minfilter, GLenum magfilter
){
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minfilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magfilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, x, y, 0, format, type, nullptr);

	ASSERT_NO_GLERR();
}

static void createImageTexture(GLuint& texid, uint x, uint y, UCHAR* data){
	glGenTextures(1, &texid);
	glBindTexture(GL_TEXTURE_2D, texid);
	CHECKED_GL_CALL(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, x, y, 0, GL_RGB, GL_UNSIGNED_BYTE, data));
	if(data){
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	ASSERT_NO_GLERR();
}

static void initQuad(GLuint& quadVAO, GLuint& quadVBO) {
	float quadVertices[] = {
	// positions
		-1.0f,  1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f,  1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
  };
	// setup plane VAO
	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
}
