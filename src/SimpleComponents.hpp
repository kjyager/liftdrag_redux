#pragma once
#ifndef _SIMPLECOMPONENTS_H_
#define _SIMPLECOMPONENTS_H_

#include <vector>
#include <common.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include "Geometry.hpp"

using namespace std;

// This file is for components so simple they can be written as just a header

class Pose{
public:
	Pose() {}
	Pose(const glm::vec3 nloc) : loc(nloc){};
	Pose(const glm::vec3 nloc, const glm::quat nori): loc(nloc), orient(nori){};

	Pose* clone() { return(new Pose(*this)); }

	glm::mat4 getAffineMatrix(){return(glm::translate(glm::mat4(1.0), loc) * glm::toMat4(orient) * glm::scale(glm::mat4(1.0), scale) * preaffine);}

	glm::vec3 loc = glm::vec3(0.0);
	glm::quat orient;
	glm::vec3 scale = glm::vec3(1.0);

	glm::mat4 preaffine = glm::mat4(1.0);
};

class SolidMesh{
public:
	SolidMesh(vector<Geometry> &copygeom){geometrylist = copygeom;}
	SolidMesh(){}
	~SolidMesh(){}

	SolidMesh* clone() { return(new SolidMesh(*this)); }

	vector<Geometry> geometrylist;
};


class Camera{
public:
	virtual ~Camera(){};
	virtual glm::vec3 getLocation() = 0;
	virtual glm::vec3 getViewDir() = 0;
	virtual glm::mat4 getView() = 0;
	virtual glm::mat4 getPerspective(float aspect) = 0;
	virtual glm::mat4 getOrthographic() = 0;

};

class StaticCamera : public Camera{
public:
	StaticCamera() : fov(45.0f), near(.01f), far(100.0f), pose(glm::vec3(0.0)), lookat(glm::vec3(0.0)), updir(glm::vec3(0.0, 1.0, 0.0)){}
	StaticCamera(float fov, const glm::vec3 &loc, const glm::vec3 &look) : fov(fov), near(.01f), far(100.0f), pose(loc), lookat(look), updir(glm::vec3(0.0, -1.0, 0.0)){}
	StaticCamera* clone() { return(new StaticCamera(*this)); }

	glm::vec3 getViewDir(){return(lookat-pose.loc);}

	glm::vec3 getLocation() {
		return(pose.loc);
	}

	glm::mat4 getView(){
		return(glm::lookAt(
			pose.loc,
			lookat,
			updir
		));
	}

	glm::mat4 getPerspective(float aspect){
		return(glm::perspective(glm::radians(fov), aspect, near, far));
	}

	glm::mat4 getOrthographic(){
		return(glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, near, far));
	}

	glm::mat4 getOrthographic(float left, float right, float bottom, float top, float near, float far){
		return(glm::ortho(left, right, bottom, top, near, far));
	}

	float fov;
	float near;
	float far;


	Pose pose;
	glm::vec3 lookat;
	glm::vec3 updir;
};

class OrbitCamera : public Camera{
public:
	OrbitCamera() : fov(45.0f), near(.01f), far(100.0f), pose(glm::vec3(0.0)), lookat(glm::vec3(0.0)), updir(glm::vec3(0.0, 1.0, 0.0)){}
	OrbitCamera(float fov, const glm::vec3 &loc, const glm::vec3 &look) : fov(fov), near(.01f), far(100.0f), pose(loc), lookat(look), updir(glm::vec3(0.0, -1.0, 0.0)){}
	OrbitCamera* clone() { return(new OrbitCamera(*this)); }

	glm::vec3 getViewDir(){return(normalize(lookat-pose.loc));}

	glm::vec3 getLocation() {
		return(pose.loc);
	}

	glm::mat4 getView(){
		return(glm::lookAt(
			pose.loc,
			lookat,
			updir
		));
	}

	void updateSpin(const glm::vec2& dragvector) {
		glm::quat xrot = glm::angleAxis(-dragvector.x, glm::vec3(0.0, 1.0, 0.0));
		glm::quat yrot = glm::angleAxis(dragvector.y, glm::vec3(1.0, 0.0, 0.0));

		orbit = xrot * yrot;
		pose.loc = pose.loc * glm::mat3(orbit);
	}

	void resetUp(const glm::vec3& up){
		updir = up;
	}

	glm::mat4 getPerspective(float aspect){
		return(glm::perspective(glm::radians(fov), aspect, near, far));
	}

	glm::mat4 getOrthographic(){
		return(glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, near, far));
	}

	glm::mat4 getOrthographic(float left, float right, float bottom, float top, float near, float far){
		return(glm::ortho(left, right, bottom, top, near, far));
	}

	float fov;
	float near;
	float far;

	glm::quat orbit; 
	Pose pose;
	glm::vec3 lookat;
	glm::vec3 updir;
};

#endif
