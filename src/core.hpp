#pragma once
#ifndef LIFTDRAG_CORE_H_
#define LIFTDRAG_CORE_H_

#include <string>
#include <vector>
#include <json.hpp>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include "util/common.h"
#include "util/ShaderLibrary.hpp"

class Application{
public:
	GLFWwindow* window = nullptr; // Main application window
	int w_width = 1024;
	int w_height = 1024;
	ShaderLibrary shaderlib;

	nlohmann::json slice_shader_array;

	Application(int argc, char** argv);
	void parseArgs(int argc, char** argv);
	void initGL();
	void initGLFW();
	nlohmann::json initShaders();
};

#endif 