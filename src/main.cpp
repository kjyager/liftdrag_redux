#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <json.hpp>
#include <thread>
#include <glm/glm.hpp>

using namespace std;
using json = nlohmann::json;

#include "core.hpp"
#include "Graphics.hpp"
#include "Geometry.hpp"
#include "SimpleComponents.hpp"

#define NUMSLICES 34

// Application class is defined in core.hpp

Application::Application(int argc, char** argv){
	parseArgs(argc, argv);
	initGLFW();
	initGL();
	json specialshaders = initShaders();
	Graphics::init(this, NUMSLICES);
	Graphics::printRelevantConstants();
	Graphics::initSpecialShaders(specialshaders);
	glfwSwapBuffers(window);
	glfwPollEvents();
}

void Application::parseArgs(int argc, char** argv){
	for(int i = 1; i < argc; i++){
		string arg = string(argv[i]);
		// We don't actually need to do anything so far in this application
	}
}

void Application::initGL(){
	if (!gladLoadGL())
	{
		std::cerr << "Failed to initialize GLAD" << std::endl;
	exit(1);
	}
	glGetError();

	fprintf(stderr, "ARB_CLEAR_TEXTURE: %s\n", GL_ARB_clear_texture ? "true" : "false");

	// cout << "OpenGL version: " << glGetString(GL_VERSION) << endl;
	// cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

	// glEnable(GL_CULL_FACE);
	// glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glClear(GL_COLOR_BUFFER_BIT);
}

void Application::initGLFW(){
	auto error_callback = [](int error, const char* description){cerr << description << endl;};
	auto key_callback = [](GLFWwindow* window, int key, int scancode, int action, int mods){
		if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS){
			glfwSetWindowShouldClose(window, true);
    }
	};

	glfwSetErrorCallback(error_callback);

	if(!glfwInit()) {
		exit(-1);
	}
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	window = glfwCreateWindow(w_width, w_height, "Liftdrag_redux", NULL, NULL);
	if(!window) {
		glfwTerminate();
		exit(-1);
	}

	glfwSetKeyCallback(window, key_callback);

	glfwMakeContextCurrent(window);

	glfwSwapInterval(1);
	glfwSetFramebufferSizeCallback(window, Graphics::onResize);
}

/** This is my own shader managment system. It's a bit messy at this point because it's seen
 * several iterations, but no refactoring. Hopefully you can ignore it for the most part, I'll point out
 * any uses of it later in the code.
 */
json Application::initShaders(){
	shaderlib.init();

	ifstream shaderfile = ifstream("" STRIFY(SHADER_DIR) "/shaders.json");
	if(!shaderfile.is_open()){
		fprintf(stderr, "Failed to open shaders json file!\n");
		exit(3);
	}
	json shaderjson;
	shaderfile >> shaderjson;
	for(json::iterator it = shaderjson["pairs"].begin(); it != shaderjson["pairs"].end(); it++){
		shaderlib.buildAndAdd(it.key(), it.value());
		cout << "Loaded shader: " << it.key() << endl;
	}
	for (json::iterator it = shaderjson["chains"].begin(); it != shaderjson["chains"].end(); it++) {
		shaderlib.buildAndAdd(it.key(), it.value());
		cout << "Loaded shader: " << it.key() << endl;
	}
	return(shaderjson["special"]);
}

/** The flow of this version of the code forms the outline for every frame, then calls the visualization
 * to see it as a point cloud. This doesn't allow you to watch things go slice by slice like you wanted, but 
 * can be changed to behave that way if you look inside formOutline. The meat of the code is all inside Graphics.*pp
 * which is a namespace with static member variables, which lets it act as a singleton. Hopefully this makes it  
 * easy to re-locate and re-organize the graphics code as you see fit. 
 */

int main(int argc, char** argv){
	Application application(argc, argv);
	SolidMesh wing;
	Geometry::loadFullObj("" STRIFY(ASSET_DIR) "sphere_highRes.obj", wing.geometrylist);

	while(!glfwWindowShouldClose(application.window)){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Graphics::formOutline(wing, NUMSLICES, vec3(0, 0, 1));
		Graphics::visualizeOutline(0.0, wing);
		// Graphics::testVisual();
		Graphics::cleanOutline();

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	glfwHideWindow(application.window);

	glfwDestroyWindow(application.window);
	glfwTerminate();
	
	return 0;
}